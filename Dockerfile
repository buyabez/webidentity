FROM golang:1.13 as builder

WORKDIR /go/src/buyabez/webidentity
COPY . /go/src/buyabez/webidentity

RUN CGO_ENABLED=0 GOOS=linux go build

FROM alpine:latest
RUN apk --no-cache add ca-certificates
COPY --from=builder /go/src/buyabez/webidentity/webidentity /usr/bin/webidentity 
RUN chmod +x /usr/bin/webidentity
ENTRYPOINT ["./usr/bin/webidentity"]
