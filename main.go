package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
)

func lookupAccount(url string) (bool, error) {
	resp, err := http.Get(url)

	if err != nil {
		return false, err
	}

	if resp.StatusCode == 200 {
		return false, nil // site/account exists
	}

	return true, nil
}

func main() {
	user := os.Args[1]

	fmt.Printf("Checking availability of account name %s\n", user)

	urls := map[string]string{
		"Twitter":   "https://twitter.com/%s",
		"Github":    "https://github.com/%s",
		"GitLab":    "https://gitlab.com/%s",
		"HackerOne": "https://hackerone.com/%s",
		"Docker":    "https://hub.docker.com/u/%s",
	}

	for site, url := range urls {
		state := "\u2612"
		url = fmt.Sprintf(url, user)

		ok, err := lookupAccount(url)

		if err != nil {
			log.Fatal(err)
		}

		if ok {
			state = "\u2611"
		}

		fmt.Printf("  %s %s (%s)\n", state, site, url)
	}
	fmt.Println()
}
